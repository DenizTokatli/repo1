import {NewsService} from './news.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

describe('NewsService', () => {
  const mockHttpClient: HttpClient = jasmine.createSpyObj('HttpClient', ['get', 'post']);
  const service: NewsService = new NewsService(mockHttpClient);
  const baseUrl = `https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=1fc8d63274c4488caf7dc65fef32720b`;

  it('should get News', () => {
    const mockResult = Observable.create([]);
    (<jasmine.Spy>mockHttpClient.get).and.returnValue(mockResult);

    const actualResult = service.getNews();
    expect(mockHttpClient.get).toHaveBeenCalledWith(baseUrl);
    expect(actualResult).toEqual(mockResult);
  });

});
