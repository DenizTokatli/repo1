import {ActivityService} from './activity.service';
import { HttpClient } from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Activity} from '../shared/models/activity';

describe('ActivityService', () => {
    const mockHttpClient: HttpClient = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    const service: ActivityService = new ActivityService(mockHttpClient);
    const baseUrl = 'http://localhost:8080/activities';

    it('should findAll activity', () => {
        const mockResult = Observable.create([]);
        (<jasmine.Spy>mockHttpClient.get).and.returnValue(mockResult);

        const actualResult = service.findAll();
        expect(mockHttpClient.get).toHaveBeenCalledWith(baseUrl);
        expect(actualResult).toEqual(mockResult);
    });

    it('should insert activity', () => {
        const mockResult = Observable.create(new Activity(0, 'abc'));
        (<jasmine.Spy>mockHttpClient.post).and.returnValue(mockResult);

        const actualResult = service.insert(new Activity(0, 'abc'));
        expect(mockHttpClient.post).toHaveBeenCalled();
        expect(actualResult).toEqual(mockResult);
    });

    }
);
