import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  API_KEY = '1fc8d63274c4488caf7dc65fef32720b';
  constructor(private httpClient: HttpClient) { }

  public getNews(): Observable<any> {
    return this.httpClient.get(`https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=${this.API_KEY}`);
  }
}
