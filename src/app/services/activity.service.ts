import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Activity} from '../shared/models/activity';
import {Observable} from 'rxjs';

const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' })};
const baseUrl = 'http://localhost:8080/activities';

@Injectable({ providedIn: 'root' })
export class ActivityService {

  constructor(private http: HttpClient) {}

  public findAll(): Observable<Activity[]> { return this.http.get<Activity[]>(baseUrl); }

  public insert(activity: Activity): Observable<Activity> { return this.http.post<Activity>(baseUrl, activity, httpOptions); }

}
