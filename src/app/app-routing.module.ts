import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsComponent } from './news/news.component';
import { ActivityComponent } from './activity/activity.component';
import { ActivityNewComponent } from './activity-new/activity-new.component';

const routes: Routes = [
    { path:  '', redirectTo:  '/news', pathMatch:  'full' },
    { path: 'activities', component: ActivityComponent },
    { path: 'activity-new', component: ActivityNewComponent },
    { path: 'news', component: NewsComponent }
];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
