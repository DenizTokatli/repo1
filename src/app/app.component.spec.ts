import {TestBed, ComponentFixture, getTestBed} from '@angular/core/testing';
import { AppComponent } from './app.component';
import {RouterTestingModule} from '@angular/router/testing';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;

  beforeEach( () => {
      TestBed.configureTestingModule({
          declarations: [AppComponent],
          imports: [RouterTestingModule.withRoutes([])],
      }).compileComponents();
      fixture = TestBed.createComponent(AppComponent);
      fixture.detectChanges();
  });

  it('should create the app', () => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'routine-tracker'`, () => {
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Welcome to Routine Tracker!');
  });

  it('should render title in a h1 tag', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to Routine Tracker!');
  });

});
