import { Component, OnInit } from '@angular/core';
import { NewsService } from '../services/news.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html'
})
export class NewsComponent implements OnInit {
  articles;

  constructor(private apiService: NewsService) { this.getNews(); }

  ngOnInit() {
    // this.apiService.getNews().subscribe((data) => {
    //   console.log(data);
    //   this.articles = data['articles'];
    // });
}

  getNews(): void {
    this.apiService.getNews().subscribe((data) => {
      console.log(data);
      this.articles = data['articles'];
    });
  }

}
