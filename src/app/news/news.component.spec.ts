import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsComponent } from './news.component';
import {NewsService} from '../services/news.service';
import {FormsModule} from '@angular/forms';
import {Observable, of} from 'rxjs';

describe('NewsComponent', () => {
  let component: NewsComponent;
  let fixture: ComponentFixture<NewsComponent>;
  const mockService: NewsService = jasmine.createSpyObj('NewsService', {'getNews': Observable.create([])});

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ], providers: [{provide: NewsService, useValue: mockService}], declarations: [NewsComponent]
    }).compileComponents();
    fixture = TestBed.createComponent(NewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get News', () => {
    (<jasmine.Spy>mockService.getNews).and.returnValue(Observable.create([0, ''])); // spying on mock component
    spyOn(component, 'getNews').and.callThrough(); // spying on real component
    fixture.ngZone.run(() => {
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(component.getNews).toHaveBeenCalled();
        expect(mockService.getNews).toHaveBeenCalled();
        expect(component.articles).toEqual(Observable.create([0, '']));
      });
    });
  });

});
