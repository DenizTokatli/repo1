import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ActivityNewComponent} from './activity-new.component';
import {ActivityService} from '../services/activity.service';
import {FormsModule} from '@angular/forms';
import {Observable} from 'rxjs';
import {Activity} from '../shared/models/activity';
import {Router} from '@angular/router';

describe('ActivityNewComponent', () => {
  let component: ActivityNewComponent;
  let fixture: ComponentFixture<ActivityNewComponent>;
  const mockService: ActivityService = jasmine.createSpyObj('ActivityService', ['insert']);
  const mockRouter: Router = jasmine.createSpyObj<Router>('Router', ['navigate']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ],
      providers: [{provide: ActivityService, useValue: mockService}, {provide: Router, useValue: mockRouter}],
      declarations: [ActivityNewComponent]
    }).compileComponents();
    fixture = TestBed.createComponent(ActivityNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should insert Activity and navigate back', () => {
    (<jasmine.Spy>mockService.insert).and.returnValue(Observable.create([0, ''])); // spying on mock component
    spyOn(component, 'insertActivity').and.callThrough(); // spying on real component
    fixture.detectChanges();

    const activity = new Activity(0, 'aaa')
    component.activityName = activity.name
    const button = fixture.nativeElement.querySelector('#btnInsert');
    button.click();

    expect(component.insertActivity).toHaveBeenCalled();
    expect(mockService.insert).toHaveBeenCalledWith(activity);
  });

});
