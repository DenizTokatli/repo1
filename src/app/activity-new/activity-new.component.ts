import { Component, OnInit } from '@angular/core';
import {Activity} from '../shared/models/activity';
import {ActivityService} from '../services/activity.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-activity-new',
  templateUrl: './activity-new.component.html'
})
export class ActivityNewComponent implements OnInit {
  activityName: string;

  constructor(private activityService: ActivityService, private router: Router) {}

  ngOnInit(): void {
  }

  insertActivity(): void {
    this.activityService.insert(new Activity(0, this.activityName)).subscribe(data => {
      console.log('logged successfully' + data.name);
      this.router.navigate(['activities']);
    });
  }

}
