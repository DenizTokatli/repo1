import {ComponentFixture,  TestBed} from '@angular/core/testing';
import { ActivityComponent } from './activity.component';
import { ActivityService } from '../services/activity.service';
import {FormsModule} from '@angular/forms';
import {Observable, of} from 'rxjs';

describe('ActivityComponent', () => {
  let component: ActivityComponent;
  let fixture: ComponentFixture<ActivityComponent>;
  const mockService: ActivityService = jasmine.createSpyObj('ActivityService', {'findAll': Observable.create([])});

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ], providers: [{provide: ActivityService, useValue: mockService}], declarations: [ActivityComponent]
    }).compileComponents();
    fixture = TestBed.createComponent(ActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
      expect(component).toBeTruthy();
  });

  it('should findAll Activity', () => {
    (<jasmine.Spy>mockService.findAll).and.returnValue(Observable.create([])); // spying on mock component
    spyOn(component, 'findAllActivity').and.callThrough(); // spying on real component
    fixture.ngZone.run(() => {
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(component.findAllActivity).toHaveBeenCalled();
        expect(mockService.findAll).toHaveBeenCalled();
        expect(component.activities).toEqual(Observable.create([]));
      });
    });
  });



});
