import {Component, OnInit} from '@angular/core';
import {ActivityService} from '../services/activity.service';
import {Activity} from '../shared/models/activity';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html'
})
export class ActivityComponent implements OnInit {
  activities: Activity[];

  constructor(private activityService: ActivityService) {}

  ngOnInit(): void {
    this.findAllActivity();
  }

  findAllActivity(): void {
    this.activityService.findAll().subscribe((data) => {
      this.activities = data; // console.log(data);
    });
  }

}
